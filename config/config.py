from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options as firefoxoptions

#Set Headless mode
chrome_options = Options()
chrome_options.headless = True
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
firefox_options = firefoxoptions()
firefox_options.headless = True
firefox_options.add_argument('--headless')
firefox_options.add_argument('--no-sandbox')
firefox_options.add_argument('--disable-dev-shm-usage')

#Start webdriver

class Driver(object):
    Instance = webdriver

    def initialize_chrome(self):
        Instance = webdriver.Chrome(chrome_options=chrome_options)
        Instance.implicitly_wait(time_to_wait=3)
        Instance.maximize_window()
        Instance.implicitly_wait(time_to_wait=3)
        print('Started Chrome Browser Session')
        return Instance

    def initialize_firefox(self):

        Instance = webdriver.Firefox(firefox_options=firefox_options)
        Instance.implicitly_wait(time_to_wait=3)
        Instance.maximize_window()
        Instance.implicitly_wait(time_to_wait=3)
        print('Started Firefox Browser Session')
        return Instance

    def close_firefox_driver(self):
        Instance = webdriver.Firefox(firefox_options=firefox_options)
        Instance.close()
        print('Ending FireFox Browser Session')

    def close_chrome_driver(self):
        Instance = webdriver.Chrome(chrome_options=chrome_options)
        Instance.close()
        print('Ending Chrome Browser Session')

from rbzautomation.config.config import Driver
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import pyotp


_url="https://qa001.app.reblaze.io/signin"
#Correct login vars
_email="yana@reblaze.com"
_password= "Ezalber!@34"
_otp_seed="Y7HTW6QYVDOTLGCL" #OTP seed for yana@reblaze.com email

#Incorrect login vars
_incorrect_email="test@test.com"
_incorrect_password="test1234"
_invalid_opt=123456


#LOGIN locators
_login_page='//*[@id="login-form"]'
_email_field ='email'
_password_field='password'
_otp_field='reblazekeyotp'
_text_my_code='send-sms' #BY id
_login_btn='//*[@id="login-form"]/button' #BY xpath
_forgot_pass='//*[@id="login-form"]/a' #BY xpath


#DASHBOARD locator
_dashboard='Dashboard'


class login(object):

    def get_otp(self):
        seed=pyotp.TOTP(_otp_seed).now()
        return seed

    def valid_login(self):
        #add email address
        element=Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(_url)
        enterMail=element.find_element(By.NAME, _email_field)
        enterMail.clear()
        enterMail.send_keys(_email)
        enterPass=element.find_element(By.NAME,_password_field)
        enterPass.clear()
        enterPass.send_keys(_password)
        otp=self.get_otp
        element.implicitly_wait(time_to_wait=2)
        enterOtp=element.find_element(By.NAME, _otp_field)
        enterOtp.send_keys(otp)
        loginBtn=element.find_element(By.XPATH, _login_btn)
        loginBtn.click()
        element.implicitly_wait(time_to_wait=3)
        loggedIn=element.find_element(By.ID, _dashboard)
        assert loggedIn=='Dashboard'
        print('Login completed successfully')
        return element

    def invalid_username(self):
        element=Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(_url)
        invalidMail=element.find_element(By.NAME,_email_field)
        invalidMail.clear()
        invalidMail.send_keys(_incorrect_email)
        enterPass = element.find_element(By.NAME, _password_field)
        enterPass.clear()
        enterPass.send_keys(_password)
        otp = self.get_otp
        element.implicitly_wait(time_to_wait=2)
        enterOtp = element.find_element(By.NAME, _otp_field)
        enterOtp.send_keys(otp)
        loginBtn = element.find_element(By.XPATH, _login_btn)
        loginBtn.click()
        loginPage=element.find_element(By.NAME,_login_page)
        assert loginPage=='Log In'
        return element

    def invalid_password(self):
        element=Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(_url)
        enterMail = element.find_element(By.NAME, _email_field)
        enterMail.clear()
        enterMail.send_keys(_email)
        invalidPass=element.find_element(By.NAME,_password_field)
        invalidPass.clear()
        invalidPass.send_keys(_incorrect_password)
        otp = self.get_otp
        element.implicitly_wait(time_to_wait=2)
        enterOtp = element.find_element(By.NAME, _otp_field)
        enterOtp.send_keys(otp)
        loginBtn = element.find_element(By.XPATH, _login_btn)
        loginBtn.click()
        loginPage = element.find_element(By.NAME, _login_page)
        assert loginPage == 'Log In'
        return element

    def forgot_password(self):
        element=Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(_url)
        forgotPass=element.find_element(By.XPATH,_forgot_pass)
        assert forgotPass=='Forgot password?'
        forgotPass.click()
        print("Moving to forgot password page")
        return element





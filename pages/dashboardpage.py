from selenium import webdriver
from rbzautomation.config.config import Driver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from rbzautomation.pages.mainpage import Tabs

url='https://qa001.app.reblaze.io/'
#dashboard locators
_filter_txt='//*[@id="filter-input"]'
_filter_btn='//*[@id="filter-form"]/center/div/button'
_date_dropdown='//*[@id="reportrange"]'
_30min_filter='/html/body/div[4]/div[1]/ul/li[1]'
_1h_filter='/html/body/div[4]/div[1]/ul/li[2]'
_2h_filter='/html/body/div[4]/div[1]/ul/li[3]'
_3h_filter='/html/body/div[4]/div[1]/ul/li[4]'
_4h_filter='/html/body/div[4]/div[1]/ul/li[5]'
_12h_filter='/html/body/div[4]/div[1]/ul/li[6]'
_day_filter='/html/body/div[4]/div[1]/ul/li[7]'
_custom_range='/html/body/div[4]/div[1]/ul/li[8]'
_apply_btn='/html/body/div[4]/div[1]/div/button[1]'
_cancel_btn='/html/body/div[4]/div[1]/div/button[2]'
_start_date='/html/body/div[4]/div[2]/div[1]/input'
_end_date='/html/body/div[4]/div[3]/div[1]/input'
#dashboard tabs
_applications='/html/body/section/div[2]/div/div[1]/div/ul/li[1]/a'
_countries='/html/body/section/div[2]/div/div[1]/div/ul/li[2]/a'
_source='/html/body/section/div[2]/div/div[1]/div/ul/li[3]/a'
_sessions='/html/body/section/div[2]/div/div[1]/div/ul/li[4]/a'
_targets='/html/body/section/div[2]/div/div[1]/div/ul/li[5]/a'
_signatures='/html/body/section/div[2]/div/div[1]/div/ul/li[6]/a'
_referers='/html/body/section/div[2]/div/div[1]/div/ul/li[7]/a'
_browsers='/html/body/section/div[2]/div/div[1]/div/ul/li[8]/a'
_companies='/html/body/section/div[2]/div/div[1]/div/ul/li[9]/a'
_total_time='/html/body/section/div[2]/div/div[1]/div/ul/li[10]/a'
_rbz_time='/html/body/section/div[2]/div/div[1]/div/ul/li[11]/a'
_org_time='/html/body/section/div[2]/div/div[1]/div/ul/li[12]/a'


element=Tabs.dashboard()

class Dashboard(object):

    def filter_default(self):
        filter=element.find_element(By.XPATH, _filter_btn)
        filter.click()
        return element
    def search_403(self):
        elem=element.find_element(By.XPATH,_filter_txt)
        elem.send_keys('is:403')
        filter=element.find_element(By.XPATH,_filter_btn)
        filter.click()
        return element
    def search_200(self):
        elem = element.find_element(By.XPATH, _filter_txt)
        elem.send_keys('is:200')
        filter = element.find_element(By.XPATH, _filter_btn)
        filter.click()
        return element
    def date_dropdown(self):
        elem=element.find_element(By.XPATH,_date_dropdown)
        elem.click()
        return element
    def filter_30min(self):
        elem=element.find_element(By.XPATH,_30min_filter)
        elem.click()
        return element
    def filter_1h(self):
        elem=element.find_element(By.XPATH,_1h_filter)
        elem.click()
        return element
    def filter_2h(self):
        elem=element.find_element(By.XPATH,_2h_filter)
        elem.click()
        return element
    def filter_3h(self):
        elem=element.find_element(By.XPATH,_3h_filter)
        elem.click()
        return element
    def filter_4h(self):
        elem=element.find_element(By.XPATH,_4h_filter)
        elem.click()
        return element
    def filter_12h(self):
        elem=element.find_element(By.XPATH,_12h_filter)
        elem.click()
        return element
    def day_filter(self):
        elem=element.find_element(By.XPATH,_day_filter)
        elem.click()
        return element
    def switch_application_tab(self):
        elem=element.find_element(By.XPATH,_applications)
        elem.click()
        return element
    def switch_countries_tab(self):
        elem=element.find_element(By.XPATH,_countries)
        elem.click()
        return element
    def switch_sources_tab(self):
        elem=element.find_element(By.XPATH,_source)
        elem.click()
        return element
    def switch_session_tab(self):
        elem=element.find_element(By.XPATH,_sessions)
        elem.click()
        return element
    def switch_target_tab(self):
        elem=element.find_element(By.XPATH,_targets)
        elem.click()
        return element
    def switch_signature_tab(self):
        elem=element.find_element(By.XPATH,_signatures)
        elem.click()
        return element
    def switch_referers_tab(self):
        elem=element.find_element(By.XPATH,_referers)
        elem.click()
        return element
    def switch_browsers_tab(self):
        elem=element.find_element(By.XPATH,_browsers)
        elem.click()
        return element


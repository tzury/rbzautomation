from selenium import webdriver
from rbzautomation.config.config import Driver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

#forgot password page locators
_title='//*[@id="current-site-title"]'
_email_textbox='email'
_reset_pass_btn='//*[@id="submit-button"]'

#Variables
_url="https://qa001.app.reblaze.io/account/reset"
_email="yana@reblaze.com"
_login_page='//*[@id="login-form"]'
_empty_email=""

class forgot_password(object):

    def forgot_password(self):
        element=Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(_url)
        enterEmail=element.find_element(By.NAME,_email_textbox)
        enterEmail.clear()
        enterEmail.send_keys(_email)
        resetPass= element.find_element(By.XPATH, _reset_pass_btn)
        resetPass.click()
        element.implicitly_wait(time_to_wait=10)
        loginPage=element.find_element(By.XPATH,_login_page)
        assert loginPage=='Log In'
        print('Back to login page')
        return element

    def no_email(self):
        element=Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(_url)
        noEmailpass=element.find_element(By.NAME,_email_textbox)
        noEmailpass.clear()
        noEmailpass.send_keys(_empty_email)
        resetPass = element.find_element(By.XPATH, _reset_pass_btn)
        resetPass.click()
        ver=element.find_element(By.XPATH,_title)
        assert ver=='Reset My Password'
        return element






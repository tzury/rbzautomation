from rbzautomation.config.config import Driver
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

#Page locatores
_dashboard='Dashboard'
_view_log='View Log'
_dynamic_rules='Dynamic Rules'
_quarantined="Quarantined"
_profiles="Profiles"
_args_analysis="Args Analysis"
_session_profiling="Session Profiling"
_rate_limit="Rate Limiting"
_cloud_fun="Cloud Functions"
_web_proxy="Web Proxy"
_backend_services="Backend Services"
_error_pages="Error Pages"
_ssl="SSL"
_dns="DNS"
_planet_overview="Planet Overview"
_account="Account"
_users="Users"
_support="Support"
_api="API"

url="https://qa001.app.reblaze.io/"

class Tabs(object):

    def dashboard(self):
        element=Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        elem=element.find_element(By.ID,_dashboard)
        elem.click()
        return element

    def view_log(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _view_log)
        return element

    def dynamic_rule(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _dynamic_rules)
        return element

    def quarantined(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _quarantined)
        return element

    def profiles(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _profiles)
        return element

    def args_analysis(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _args_analysis)
        return element

    def session_profiling(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _session_profiling)
        return element

    def rate_limit(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _rate_limit)
        return element

    def cloud_functions(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _cloud_fun)
        return element

    def web_proxy(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _web_proxy)
        return element

    def backend_services(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _backend_services)
        return element

    def error_pages(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _error_pages)
        return element

    def ssl(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _ssl)
        return element

    def dns(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _dns)
        return element

    def planet_overview(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _planet_overview)
        return element

    def account(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _account)
        return element

    def users(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _users)
        return element

    def support(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _support)
        return element

    def api(self):
        element = Driver.initialize_chrome()
        element.implicitly_wait(time_to_wait=3)
        element.get(url)
        element.implicitly_wait(time_to_wait=3)
        element.find_element(By.ID, _api)
        return element

